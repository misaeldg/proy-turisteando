/*import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
*/


import { RouterModule, Routes } from '@angular/router'

import { HomeComponent } from './components/home/home.component';


const APP_ROUTES: Routes = [
    
    { path : 'home', component: HomeComponent},
    { path : '**', pathMatch:'full', redirectTo: 'home'}
]



export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, { useHash: true });
